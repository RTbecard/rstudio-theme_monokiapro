A variant of the One Monokai theme found [here](https://github.com/jcasasn/rstudio-theme-one-monokai/tree/main).

This was designed to replicate the [Monokia Pro](https://monokai.pro/vscode) theme.

![](Screenshot_from_2024-04-15_16-03-02.png)